from django.urls import path
from . import views

urlpatterns = [
    path("", views.index),
    path("configuracion", views.configuracion),
    path("ayuda", views.ayuda),
    path("<str:sala>.json",views.sala_json),
    path("<str:sala>.dinamica",views.sala_dinamica),
    path("<str:sala>",views.sala)
]