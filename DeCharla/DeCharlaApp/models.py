from django.db import models
from django.utils import timezone

# Create your models here.


class Passwords(models.Model):
    password = models.TextField()

class Sesion(models.Model):
    sesion_id = models.TextField(unique=True)
    nombre = models.TextField(default="Anonimo")
    tipoLetra = models.TextField(default="default")
    tamLetra = models.FloatField(default="3")

class Sala(models.Model):
    nombre = models.TextField()
    sesion = models.ForeignKey(Sesion, on_delete=models.SET_NULL, null=True)
    usuario = models.TextField()
    descripcion = models.TextField()

class Mensaje(models.Model):
    sesion = models.ForeignKey(Sesion, on_delete=models.SET_NULL, null=True)
    sala = models.ForeignKey(Sala, on_delete=models.CASCADE)
    msg = models.TextField()
    img = models.BooleanField()
    usuario = models.TextField()
    fecha = models.DateTimeField(default=timezone.now)

class UltimaVista(models.Model):
    sesion = models.ForeignKey(Sesion, on_delete=models.CASCADE)
    sala = models.ForeignKey(Sala, on_delete=models.CASCADE)
    ultimoAcceso = models.DateTimeField(default=timezone.now)

class MeGusta(models.Model):
    sesion = models.ForeignKey(Sesion, on_delete=models.CASCADE)
    sala = models.ForeignKey(Sala, on_delete=models.CASCADE)
    estado = models.BooleanField()

