# Final DeCharla

# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Alejandro Bernabéu Fernández
* Titulación: Grado en Ingeniería en Tecnologías de la Telecomunicación
* Cuenta en laboratorios: bernabeu
* Cuenta URJC: a.bernabeu.2018@alumnos.urjc.es
* Video básico (url): https://youtu.be/8RUgtNKkKaY
* Video parte opcional (url): https://youtu.be/eoHyEZDzLYQ
* Despliegue (url): http://bernabeu18.pythonanywhere.com/ 
* Contraseñas: SAT/bernabeu/Django/Cookie
* Cuenta Admin Site: bernabeu/bernabeu

## Resumen parte obligatoria
La aplicación nos permite chatear en diferentes salas. Cuando se entra por primera vez al sitio, habrá que introducir una contraseña válida para poder disfrutar de toda la funcionalidad. 

Cuando introducimos la contraseña aparece la pantalla principal, donde estarán todas las salas activas con el número total de mensajes y el número de mensajes desde la última visita. En la parte superior tenemos el menú, disponible en todas las páginas del sitio, donde podremos navegar por toda la aplicación. También en la parte superior está la cabecera, visible en todas las páginas, donde tendremos un enlace que nos redirige a la página principal.

En la parte derecha tenemos una cajita con el nombre de usuario que hemos elegido, o "Anónimo" en su defecto, y un formulario para introducir el nombre de una sala. Si está creada, accedemos a la sala, en otro caso la sala se crea. Esta funcionalidad también la tenemos en cada página de la aplicación.

El último apartado que tenemos en todas las páginas es el pie, donde encontraremos el número de salas activas, el número de mensajes textuales y el número de mensajes en formato imagen.

Cuando accedemos a una sala nos encontramos todos los mensajes de esa sala, con los más recientes en la parte superior. En la parte inferior tendremos un formulario donde enviaremos mensajes. Este formulario tendrá un checkbox, que si está seleccionado el mensaje será una imagen y habra que introducir su URL. Los mensajes también pueden enviarse mediante un PUT en formato XML.
Debajo de este formulario tenemos dos enlaces que nos sirven para acceder a la sala en formato JSON o para acceder a la sala dinámica, la cual se va actualizando cada 30 segundos.

Por último, hay una página de ayuda con información sobre la funcionalidad de la aplicación, una págna de configuración donde se podrá cambiar el nombre de usuario y el tamaño y tipo de letra, y la página del Admin Site de Django.
## Lista partes opcionales

* Nombre parte: Inclusión de favicon del sitio.
* Nombre parte: Logout. Se permite cerrar la sesión a los usuarios de forma que desaparezca la sesión de la base de datos.
* Nombre parte: Votar las salas. En cada sala hay dos botones que permiten votar a favor o en contra de las salas. En la página principal aparece el nombre de la sala con su número de votos.
* Nombre parte: Atención al idioma indicado por el navegador. En función del idioma que elijamos en nuestro navegador, algunos aspectos de la aplicación aparecen en ese idioma.