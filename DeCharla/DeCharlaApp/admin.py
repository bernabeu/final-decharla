from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(Passwords)
admin.site.register(Sesion)
admin.site.register(Sala)
admin.site.register(Mensaje)
admin.site.register(UltimaVista)
