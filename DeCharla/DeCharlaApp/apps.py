from django.apps import AppConfig


class DecharlaappConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "DeCharlaApp"
