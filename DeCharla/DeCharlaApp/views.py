import json
import random
import string
import xml.etree.ElementTree as ET
from django.shortcuts import render, HttpResponse, redirect
from .models import *
from django.contrib import messages
from django.views.decorators.csrf import ensure_csrf_cookie

# Create your views here.

def datos_pagina(request):
    sesion = Sesion.objects.get(sesion_id=request.COOKIES.get("cookie"))
    tipo = sesion.tipoLetra
    tam = "medium"
    if tipo == "default":
        tipo = "sans-serif"
    tama = sesion.tamLetra
    if tama == 3.0:
        tam = "medium"
    elif tama == 8.0:
        tam = "small"
    elif tama == 12.0:
        tam = "medium"
    elif tama == 16.0:
        tam = "large"
    nombre = sesion.nombre
    if nombre == "":
        nombre = "Anonimo"
    return tipo,tam,nombre

def datos_footer(request):
    msg_txt = Mensaje.objects.filter(img=False)
    num_msg_txt = len(msg_txt)
    msg_img = Mensaje.objects.filter(img=True)
    num_msg_img = len(msg_img)
    salas = Sala.objects.all()
    num_salas = len(salas)
    tipo, tam,nombre = datos_pagina(request)
    context = {
        'texto': num_msg_txt,
        'img': num_msg_img,
        'salas': num_salas,
        'nombre': nombre,
        'tipo': tipo,
        'tam': tam
    }
    return context

def datos_principal(request,context):
    listasalas = []
    listasalasaux = []
    num_msg_salas = []
    num_msg_ultima = []
    num_megusta = []
    num_nomegusta = []
    ultimas = []
    cookie = request.COOKIES.get("cookie")
    mensajes = Mensaje.objects.all()
    salas = Sala.objects.all()
    megustas = MeGusta.objects.filter(estado=True)
    nomegustas = MeGusta.objects.filter(estado=False)
    for mensaje in mensajes:
        for sala in salas:
            if mensaje.sala == sala:
                listasalas.append(sala)
    for sala in listasalas:
        if sala not in listasalasaux:
            listasalasaux.append(sala)
    for sala in listasalasaux:
        num_msg = 0
        num_mg = 0
        num_nmg = 0
        for mensaje in mensajes:
            if mensaje.sala == sala:
                num_msg += 1
        num_msg_salas.append(num_msg)
        for megusta in megustas:
            if megusta.sala == sala:
                num_mg += 1
        num_megusta.append(num_mg)
        for nomegusta in nomegustas:
            if nomegusta.sala == sala:
                num_nmg += 1
        num_nomegusta.append(num_nmg)
        if cookie:
            sesion = Sesion.objects.get(sesion_id=request.COOKIES.get("cookie"))
            visitas = UltimaVista.objects.all()
            if len(visitas)==0:
                num_msg_ultima = num_msg_salas
            else:
                for visita in visitas:
                    if visita.sesion==sesion and visita.sala==sala:
                        ultimas.append(visita)
                if len(ultimas)==0:
                    n_msg_ultima = 0
                    for mensaje in mensajes:
                        if (mensaje.sala == sala):
                            n_msg_ultima += 1
                    num_msg_ultima.append(n_msg_ultima)
                else:
                    ultima_visita = ultimas[len(ultimas)-1]
                    n_msg_ultima = 0
                    for mensaje in mensajes:
                        if (mensaje.sala == sala) and (mensaje.fecha > ultima_visita.ultimoAcceso):
                            n_msg_ultima += 1
                    num_msg_ultima.append(n_msg_ultima)
        else:
            num_msg_ultima = num_msg_salas
    datos_sala = zip(listasalasaux, num_msg_salas,num_msg_ultima,num_megusta,num_nomegusta)
    context['datos_sala'] = datos_sala
    return context


def datos_sala(context, sala):
    lista_mensajes = []
    sala_obj = Sala.objects.get(nombre=sala)
    mensajes = Mensaje.objects.all()
    for mensaje in mensajes:
        if mensaje.sala == sala_obj:
            lista_mensajes.insert(0,mensaje)
    context["mensajes"] = lista_mensajes
    return context


def validar(request):
    msg_txt = Mensaje.objects.filter(img=False)
    num_msg_txt = len(msg_txt)
    msg_img = Mensaje.objects.filter(img=True)
    num_msg_img = len(msg_img)
    salas = Sala.objects.all()
    num_salas = len(salas)
    pwd = request.POST["pass"]
    es_correcto = Passwords.objects.filter(password=pwd)
    if es_correcto:
        context = {
            'texto': num_msg_txt,
            'img': num_msg_img,
            'salas': num_salas,
            'nombre': "Anonimo",
            'tipo': "sans-serif",
            'tam': "medium"
        }
        context = datos_principal(request,context)
        resp = render(request, "principal.html",context)
        cookie = ''.join(random.choices(string.ascii_lowercase + string.digits, k=128))
        resp.set_cookie("cookie", cookie)
        sesion = Sesion(sesion_id=cookie)
        sesion.save()
        return resp
    else:
        messages.error(request,"401 Unauthorized")
        return render(request,"formulario.html")

@ensure_csrf_cookie
def index(request):
    if "boton" in request.POST:
        if request.POST["boton"] == "form":
            return validar(request)
        elif request.POST["boton"] == "accesosala":
            sesion = Sesion.objects.get(sesion_id=request.COOKIES.get("cookie"))
            existe = False
            sala = request.POST["sala"]
            salas = Sala.objects.all()
            for chat in salas:
                if chat.nombre == sala:
                    existe = True
            if not existe:
                sala_obj = Sala(nombre=sala.lower(),sesion=sesion,usuario=sesion.nombre)
                sala_obj.save()
            context = datos_footer(request)
            context['nombre_sala'] = sala.capitalize
            context = datos_sala(context, sala)
            return render(request, "sala.html", context)
        elif request.POST["boton"] == "cerrar_sesion":
            resp = render(request, "formulario.html")
            sesion = Sesion.objects.get(sesion_id=request.COOKIES.get("cookie"))
            sesion.delete()
            resp.delete_cookie("cookie")
            return resp
    elif request.method == "GET":
        cookie = request.COOKIES.get("cookie")
        if not cookie:
            return render(request, "formulario.html")
        else:
            context = datos_footer(request)
            context = datos_principal(request,context)
            return render(request, "principal.html",context)

@ensure_csrf_cookie
def ayuda(request):
    cookie = request.COOKIES.get("cookie")
    if not cookie:
        return render(request, "formulario.html")
    else:
        context = datos_footer(request)
        return render(request, "ayuda.html",context)

@ensure_csrf_cookie
def configuracion(request):
    cookie = request.COOKIES.get("cookie")
    if not cookie:
        return render(request, "formulario.html")
    else:
        context = datos_footer(request)
        if request.method == "GET":
            return render(request, "configuracion.html",context)
        elif request.method == "POST":
            nombre = request.POST["nombre"]
            tipo = request.POST["tipo"]
            tam = request.POST["tamano"]
            sesion = Sesion.objects.get(sesion_id=request.COOKIES.get("cookie"))
            sesion.nombre = nombre
            sesion.tipoLetra = tipo
            sesion.tamLetra= int(tam)
            sesion.save()
            return redirect("/")

@ensure_csrf_cookie
def sala(request,sala):
    cookie = request.COOKIES.get("cookie")
    if not cookie:
        return render(request, "formulario.html")
    else:
        sesion = Sesion.objects.get(sesion_id=request.COOKIES.get("cookie"))
        if sesion.nombre == "":
            sesion.nombre = "Anonimo"
        sala_obj = Sala.objects.get(nombre=sala)
        if request.method == "GET":
            context = datos_footer(request)
            context['nombre_sala']=sala.capitalize
            context = datos_sala(context, sala)
            ultima = UltimaVista(sesion=sesion, sala=sala_obj)
            ultima.save()
            return render(request, "sala.html",context)
        elif "boton" in request.POST:
            if request.POST["boton"] == "boton_msg":
                msg = request.POST["msg"]
                if "img" in request.POST:
                    img = True
                else:
                    img =False
                mensaje = Mensaje(sesion=sesion,sala=sala_obj,msg=msg,img=img,usuario=sesion.nombre)
                mensaje.save()
                ultima = UltimaVista(sesion=sesion, sala=sala_obj)
                ultima.save()
                context = datos_footer(request)
                context['nombre_sala'] = sala.capitalize
                context = datos_sala(context, sala)
                return render(request, "sala.html",context)
            elif request.POST["boton"] == "megusta":
                me_gusta = MeGusta(sesion=sesion,sala=sala_obj,estado=True)
                me_gusta.save()
                ultima = UltimaVista(sesion=sesion, sala=sala_obj)
                ultima.save()
                context = datos_footer(request)
                context['nombre_sala'] = sala.capitalize
                context = datos_sala(context, sala)
                return render(request, "sala.html", context)
            elif request.POST["boton"] == "nomegusta":
                me_gusta = MeGusta(sesion=sesion,sala=sala_obj,estado=False)
                me_gusta.save()
                ultima = UltimaVista(sesion=sesion, sala=sala_obj)
                ultima.save()
                context = datos_footer(request)
                context['nombre_sala'] = sala.capitalize
                context = datos_sala(context, sala)
                return render(request, "sala.html", context)
        elif request.method == "PUT":
            xml_msg = request.body.decode('utf-8')
            xml_msg = xml_messages(xml_msg)
            for msg in xml_msg:
                mensaje = Mensaje(sesion=sesion, sala=sala_obj, msg=msg["text_msg"], img=msg["img"], usuario=sesion.nombre)
                mensaje.save()
            context = datos_footer(request)
            context['nombre_sala'] = sala.capitalize
            context = datos_sala(context, sala)
            return render(request, "sala.html", context)


def sala_json(request, sala):
    cookie = request.COOKIES.get("cookie")
    if not cookie:
        return render(request, "formulario.html")
    else:
        sesion = Sesion.objects.get(sesion_id=request.COOKIES.get("cookie"))
        lista_json = []
        sala_obj = Sala.objects.get(nombre=sala)
        mensajes = Mensaje.objects.filter(sala=sala_obj)
        for mensaje in mensajes:
            msg_json = {"author": mensaje.usuario, "text":mensaje.msg, "is_image": mensaje.img, "date":mensaje.fecha.isoformat()}
            lista_json.insert(0,msg_json)
        return HttpResponse(json.dumps(lista_json), content_type="application/json")

def xml_messages(xml_msg):
    root = ET.fromstring(xml_msg)
    messages = []
    for msg in root.findall('message'):
        img = msg.get('img')
        if img == "True" or img == "true":
            img = True
        else:
            img = False
        text_msg = msg.find('text_msg').text
        messages.append({'img': img, 'text_msg': text_msg})
    return messages

def sala_dinamica(request, sala):
    cookie = request.COOKIES.get("cookie")
    url = "/"+sala+".json"
    if not cookie:
        return render(request, "formulario.html")
    else:
        sesion = Sesion.objects.get(sesion_id=request.COOKIES.get("cookie"))
        if sesion.nombre == "":
            sesion.nombre = "Anonimo"
        sala_obj = Sala.objects.get(nombre=sala)
        if request.method == "GET":
            context = datos_footer(request)
            context['nombre_sala'] = sala.capitalize
            context = datos_sala(context, sala)
            context['url'] = url
            ultima = UltimaVista(sesion=sesion, sala=sala_obj)
            ultima.save()
            return render(request, "sala_dinamica.html", context)
        elif request.method == "POST":
            msg = request.POST["msg"]
            if "img" in request.POST:
                img = True
            else:
                img = False
            mensaje = Mensaje(sesion=sesion, sala=sala_obj, msg=msg, img=img, usuario=sesion.nombre)
            mensaje.save()
            ultima = UltimaVista(sesion=sesion, sala=sala_obj)
            ultima.save()
            context = datos_footer(request)
            context['nombre_sala'] = sala.capitalize
            context = datos_sala(context, sala)
            context['url'] = url
            return render(request, "sala_dinamica.html", context)
        elif request.method == "PUT":
            xml_msg = request.body.decode('utf-8')
            xml_msg = xml_messages(xml_msg)
            for msg in xml_msg:
                mensaje = Mensaje(sesion=sesion, sala=sala_obj, msg=msg["text_msg"], img=msg["img"],
                                  usuario=sesion.nombre)
                mensaje.save()
            context = datos_footer(request)
            context['nombre_sala'] = sala.capitalize
            context = datos_sala(context, sala)
            context['url'] = url
            return render(request, "sala_dinamica.html", context)
